jQuery BootAlert
================

BootAlert is a simple function that can generate alert windows based on [Twitter Bootstrap](http://twitter.github.com/bootstrap/) css framework

Requirements
-------------

This jQuery function require the following files included in your project

* [Twitter Bootstrap css files](http://twitter.github.com/bootstrap/) 
* [Twitter Bootstrap modal javascript](http://twitter.github.com/bootstrap/javascript.html#modals) 
* [jQuery](http://jquery.com)

Installation
-------------

Place the js file inside your `<head>` tag of html

```html
<script src="js/jquery-bootalert.js"></script>
```

Usage
-----
To show an alertbox simply use 

```javascript
$.bootalert("Success", "Your message has been sent!");
```
BootAlert optionally accept third parameter for the button class value. This class name is same as the [Bootstrap Button Classes](http://twitter.github.com/bootstrap/base-css.html#buttons)

The available button classes are 

* `btn-primary`
* `btn-info`
* `btn-success`
* `btn-warning`
* `btn-danger`
* `btn-inverse`
* `btn-link`

To call the alertbox with custom button, simple add the button class name to the parameter list

```javascript
$.bootalert("Success", "Your message has been sent!", "btn-danger");
```
Live Demo
----------

[Click here](http://getvivekv.bitbucket.org/BootAlert) to see the demo

Got something to add to this script?
-------------------------------------

[Fork the git repository](https://bitbucket.org/getvivekv/bootalert/fork) and clone it. Then make a new branch and make your modifications. Once completed, commit to your repository and make a pull request.

Issues or Suggestions?
-----------------------
Post on the [Issues](https://bitbucket.org/getvivekv/bootalert/issues) page

Contact Author
---------------

[Contact Vivek](https://bitbucket.org/account/notifications/send/?receiver=getvivekv)

[Website](http://vivekv.com)
[Blog](http://blog.vivekv.com)
